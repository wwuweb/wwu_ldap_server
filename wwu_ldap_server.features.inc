<?php
/**
 * @file
 * wwu_ldap_server.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wwu_ldap_server_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ldap_servers" && $api == "ldap_servers") {
    return array("version" => "1");
  }
}
